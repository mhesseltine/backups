﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeGhost : MonoBehaviour
{
    public float pd = 1;
    public float spd = 3f;
    public Player target;

    private Room roomEnemyIsIn;

	// Use this for initialization
	void Start()
    {
        roomEnemyIsIn = this.transform.parent.GetComponent<Room>();     //The time ghost will always be a child of the room
       //target = Player.S;
       // Debug.Log(target.transform.position);
        //playerDamage = pd;
        //enemySpeed = spd;
        //tgt = Player.S;
        //base.Start();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(roomEnemyIsIn.playerInRoom == true)
        {
            target = Player.S;
            Vector3 posToGo = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
            this.transform.position = Vector3.MoveTowards(this.transform.position, posToGo, spd * Time.deltaTime);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) 
    {
        //Getting hit with a weapon will destroy this game object
        if(collision.gameObject.tag == "Weapon")
        {
            roomEnemyIsIn.enemiesInRoom.Remove(this.gameObject);    //Remove it from the enemyIsInRoom list associated with the Room this is a child of
            Destroy(this.gameObject);
        }
            
    }
}
