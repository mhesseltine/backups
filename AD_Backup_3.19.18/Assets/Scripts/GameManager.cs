﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private int maxRooms = 10;              //Maximum number of rooms
    
    public int lastX;                       //These two variables keep track of the x and y coords... 
    public int lastY;                       //...of the room that was most recently placed

    public static GameManager S;            //GameManager singleton
    public List<Room> rooms;                //List of rooms being generated
    public List<GameObject> roomPrefabs;    //Prefabs of room types

    public Player playerPrefab;

    public Camera mainCam;

    public Room FurthestFromOrigin;

    [HideInInspector]
    public List<bool> doorDirections;
    [HideInInspector]
    public int currRoom = 0;               //Room number. Incremented when a new room is instantiated

    //public GameObject test;

    // Update is called once per frame
    private void Awake()
    {
        mainCam = Camera.main;
        S = this;                           //Assign the singleton to this game object
        lastX = 0;                          //Keeps track of the last room xCoord
        lastY = 0;                          //Keeps track of the last room yCoord
        rooms = new List<Room>();           //List of all rooms on board

        doorDirections = new List<bool>();  
        for (int i = 0; i < 4; i++)
        {
            doorDirections.Add(false);
        }
        InstantiateRooms();
        BridgeRooms();

        currRoom = 0;
        rooms[currRoom].playerInRoom = true;       //Make the first room initialized (at the origin) the room the player is in

        GameObject playerInstance = Instantiate(playerPrefab.gameObject, new Vector3(0, 1, -3f), Quaternion.identity) as GameObject;
        DetermineFurthestRoom();

        foreach (var room in rooms)
        {
            if (room.finalRoom == true)
                room.SpawnBoss();
            else if (room.xCoord == 0 && room.yCoord == 0)
                ;
            else
                room.SpawnEnemies();
        }
    }
    
    private void DetermineFurthestRoom()
    {
        int indexOfFurthest = 0;
        float distance = 0;
        for(int i = 0; i < rooms.Count; i++)
        {
            if (ManhattanDistance(rooms[i].xCoord, rooms[i].yCoord) > distance)
            {
                distance = ManhattanDistance(rooms[i].xCoord, rooms[i].yCoord);
                indexOfFurthest = i;
            }

        }

        rooms[indexOfFurthest].finalRoom = true;
    }

    private float ManhattanDistance(int room_x, int room_y)
    {
        float distance = Mathf.Abs(0 - room_x) + Mathf.Abs(0 - room_y);
        return distance;
    }

    public void PlayerChangedRooms(int x, int y)
    {
        //Called when the player changes rooms.
        //The currRoom is updated.
        rooms[currRoom].playerInRoom = false;
        int index = 0;
        foreach(var room in rooms)
        {
            if (room.xCoord == x && room.yCoord == y)
            {
                currRoom = index;
                rooms[currRoom].playerInRoom = true;
                break;
            }
            else
                index++;
        }
    }

    private void Update()
    {
        if(Player.S.playerHealth <= 0)
        {
            GameOver();
        }
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    rooms[currRoom].SpawnEnemies();
           
        //    //thing.transform.parent = rooms[currRoom].transform;
        //}
    }

    private void InstantiateRooms()
    {
        //Instantiates all rooms up to the value of maxRooms.
        for (int i = 0; i < maxRooms; i++)
        {
            Instantiate(roomPrefabs[0]);
        }
    }

    private void BridgeRooms()
    {
        //Checks the x and y coordinates of all rooms. Determines if a door needs to be placed in any given direction for adjacent rooms
        for (int i = 0; i < rooms.Count; i++)
        {
            for (int j = 0; j < rooms.Count; j++)
            {
                if ((rooms[i].xCoord == rooms[j].xCoord && rooms[i].yCoord == rooms[j].yCoord - 1))         //Is there a room to the north?
                    doorDirections[0] = true;
                else if ((rooms[i].xCoord == rooms[j].xCoord && rooms[i].yCoord == rooms[j].yCoord + 1))    //Is there a room to the south?
                    doorDirections[1] = true;
                else if ((rooms[i].xCoord == rooms[j].xCoord - 1 && rooms[i].yCoord == rooms[j].yCoord))    //Is there a room to the east
                    doorDirections[2] = true;
                else if ((rooms[i].xCoord == rooms[j].xCoord + 1 && rooms[i].yCoord == rooms[j].yCoord))    //Is there a room to the west
                    doorDirections[3] = true;
            }
            rooms[i].Setup(doorDirections[0], doorDirections[1], doorDirections[2], doorDirections[3]);
            rooms[i].TranslateRoom();

            for (int k = 0; k < doorDirections.Count; k++)  //Reinitialize whether or not there is a door in any given direction
                doorDirections[k] = false;
        }
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene("Main");
    }

    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

}
