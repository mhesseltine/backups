﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]

public class Player : MonoBehaviour
{
    public static Player S;            //Player singleton
    private float playerSpeed = 4f;
    public int playerHealth = 3;
    public bool transitioningRooms = false;
    public List<GameObject> playerProjectiles;// = new List<GameObject>();
    public List<Image> healthClocks;

    public int RoomX;   //Gets the player's X coordinate on the map
    public int RoomY;   //Gets the players Y coordinate on the map
    static bool canControl = true;

    private Vector2 targetVelocity;     //Vector for moving with user input
    private Vector2 transitionVelocity; //Vector for moving while transitioning rooms
    private string dirOfDesiredRoom = "North";    //Which direction the player is moving when transitioning rooms




    // Use this for initialization
    void Start ()
    {
        S = this;       //Set the player singleton
        RoomX = 0;
        RoomY = 0;
        canControl = true;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        //Debug.Log(transitioningRooms);
        if(canControl)
        {
            targetVelocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            GetComponent<Rigidbody2D>().velocity = targetVelocity * playerSpeed;
            if (Input.GetMouseButtonDown(0))
                CreateProjectile();
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = transitionVelocity * playerSpeed;
        }

        if (transitioningRooms == true)
        {
            CheckIfArrived();
        }
    }

    private void CheckIfArrived()
    {
        var playerR = GetComponent<Renderer>();
        //bool transitionBegin = false;
        if (!playerR.isVisible)
        {
            float delay;
            transitioningRooms = false;
            if(dirOfDesiredRoom == "North" || dirOfDesiredRoom == "South")
            {
                delay = 0.55f;
                Invoke("CamFollowPlayer", delay);
            }    
            else
            {
                delay = 0.35f;
                Invoke("CamFollowPlayer", delay);
            }

           Invoke("delayedControlReturn", delay);
           
            
        }
    }

    private void delayedControlReturn()
    {
        canControl = true;
        ToggleColliders(true, 4f);
    }

    private void CreateProjectile()
    {
        float x = this.transform.position.x;
        float y = this.transform.position.y;
        float z = this.transform.position.z;
        GameObject proj = Instantiate(playerProjectiles[0], new Vector3(x, y, z), Quaternion.identity) as GameObject;    //Instantiate desired tile

        BoxCollider2D[] playerColls = GetComponents<BoxCollider2D>();
        foreach (var collider in playerColls)
            Physics2D.IgnoreCollision(proj.GetComponent<Collider2D>(), collider);   //Make the projectile ignore the box colliders of the player
    }

    private void CamFollowPlayer()
    {
        Vector3 pos;
        switch (dirOfDesiredRoom)
        {
            case "North":
                RoomY++;    //Increment Y coordinate
                pos = new Vector3(GameManager.S.mainCam.transform.position.x, GameManager.S.mainCam.transform.position.y + 11, -10);
                break;
            case "South":
                RoomY--;    //Decrement Y coordinate
                pos = new Vector3(GameManager.S.mainCam.transform.position.x, GameManager.S.mainCam.transform.position.y - 11, -10);
                break;
            case "East":
                RoomX++;    //Increment X coordinate
                pos = new Vector3(GameManager.S.mainCam.transform.position.x + 19, GameManager.S.mainCam.transform.position.y, -10);
                break;
            case "West":
                RoomX--;    //Decrement X coordinate;
                pos = new Vector3(GameManager.S.mainCam.transform.position.x - 19, GameManager.S.mainCam.transform.position.y, -10);
                break;
            default:
                pos = new Vector3(GameManager.S.mainCam.transform.position.x - 19, 0, 0);
                break;
        }

        GameManager.S.mainCam.transform.position = pos;
        GameManager.S.PlayerChangedRooms(RoomX, RoomY);     //Updates which room the player is in
      

    }
    
    private void ToggleColliders(bool enableToggle,float speed)
    {
        playerSpeed = speed;        //Reduce speed for transition

        BoxCollider2D[] playerColls = GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D bc in playerColls)
            bc.enabled = enableToggle;
    }
    public void MoveToAdjRoom(string direction)
    {
        dirOfDesiredRoom = direction;
        transitioningRooms = true;

        ToggleColliders(false, 2f);
        //playerSpeed = 2f;        //Reduce speed for transition

        //BoxCollider2D[] playerColls = GetComponents<BoxCollider2D>();
        //foreach (BoxCollider2D bc in playerColls)
        //    bc.enabled = false;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f,0f);

        switch (direction)
        {
            case "North":
                transitionVelocity = new Vector2(0f, 1f);
                break;
            case "South":
                transitionVelocity = new Vector2(0f, -1f);
                break;
            case "East":
                transitionVelocity = new Vector2(1f, 0f);
                break;
            case "West":
                transitionVelocity = new Vector2(-1f, 0f);
                break;
            default:
                transitionVelocity = new Vector2(0f, 0f);
                break;
        }
        canControl = false;     //Remove input control from the player
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            var currentClock = GameObject.Find("Health_" + (playerHealth - 1).ToString());
           // Debug.Log(currentClock);
            Destroy(currentClock);
            this.playerHealth--;    //Decrement player health
        }
    }
}
