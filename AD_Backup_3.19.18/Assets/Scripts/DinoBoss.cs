﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoBoss : MonoBehaviour
{
    public float pd = 3;
    public float spd = 1f;
    public float health = 20;

    public float leftBound = 4;
    public float rightBound = 15;

    public List<GameObject> enemyProjs;

    private Room roomBossIsIn;
    public bool sideMovement = true;
    private bool chargeAnimation = false;
    
    private Player target;
    private Vector3 posToGo;
    private Vector3 originalPos;

    bool destinationReached = false;
    bool returnToOrigin = false;
    bool playerCoordsCaptured = false;

    private float playerX;
    private float playerY;
    private float playerZ;

    


    public int rng = 0;
    public static DinoBoss S;            //DinoBoss singleton

    // Use this for initialization
    void Start ()
    {
        S = this;
        roomBossIsIn = this.transform.parent.GetComponent<Room>();     //The time ghost will always be a child of the room
        
        InvokeRepeating("CalculateChanceToCharge", 1f, 1f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        target = Player.S;
       
        if (roomBossIsIn.playerInRoom == true)
        {
            if(sideMovement == true)
            {
                destinationReached = false;
                returnToOrigin = false;
                playerCoordsCaptured = false;
                Vector3 pos = transform.localPosition;
                pos.x += spd * Time.deltaTime;
                transform.localPosition = pos;

                if (pos.x < leftBound)
                    spd = Mathf.Abs(spd);   //Move right
                else if (pos.x > rightBound)
                    spd = -Mathf.Abs(spd);  //Move left
            }

            
            
            if(rng > 75)
            {
                CancelInvoke();
                sideMovement = false;
               
                spd = 6f;
                if (playerCoordsCaptured == false)
                {
                    originalPos.x = this.transform.position.x;
                    originalPos.y = this.transform.position.y;
                    originalPos.z = this.transform.position.z;
                    playerX = target.transform.position.x;
                    playerY = target.transform.position.y;
                    playerZ = target.transform.position.z;
                    posToGo = new Vector3(playerX, playerY, playerZ);
                    playerCoordsCaptured = true;
                    //Debug.Log(posToGo);
                }
                

                if (this.transform.position == posToGo)
                    destinationReached = true;

                if (destinationReached == true)
                {
                    spd = 0;
                    returnToOrigin = true;
                }
                else
                {
                    this.transform.position = Vector3.MoveTowards(this.transform.position, posToGo, spd * Time.deltaTime);
                }
                    
                if(returnToOrigin == true)
                {
                    spd = 3f;
                    posToGo = originalPos;
                    this.transform.position = Vector3.MoveTowards(this.transform.position, posToGo, spd * Time.deltaTime);
                    if (this.transform.position == posToGo)
                    {
                        sideMovement = true;
                       
                        rng = 0;
                        InvokeRepeating("CalculateChanceToCharge", 3f, 1f);
                    }  
                }
            }

            if(rng <= 10 && rng >=0)
            {
                float x = this.transform.position.x;
                float y = this.transform.position.y + 2;
                float z = this.transform.position.z - 1;
                GameObject proj = Instantiate(enemyProjs[0], new Vector3(x, y, z), Quaternion.identity) as GameObject;    //Instantiate desired tile

                BoxCollider2D[] bossColls = GetComponents<BoxCollider2D>();
                foreach (var collider in bossColls)
                {
                    Physics2D.IgnoreCollision(proj.GetComponent<Collider2D>(), collider);   //Make the projectile ignore the box colliders of the player
                    
                }
                rng = 11;

            }
        }
    }

    private void FixedUpdate()
    {
        
    }

    private void CalculateChanceToCharge()
    {
       // Debug.Log("I have been entered");
        rng = UnityEngine.Random.Range(0, 100);
    }


    //private void SideToSide()
    //{
    //    Vector3 pos = transform.position;
    //    pos.x += spd * Time.deltaTime;
    //    transform.position = pos;

    //    if (pos.x < leftBound)
    //        spd = Mathf.Abs(spd);   //Move right
    //    else if (pos.x > rightBound)
    //        spd = -Mathf.Abs(spd);  //Move left
    //}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Getting hit with a weapon will destroy this game object
        if (collision.gameObject.tag == "Weapon")
        {
            health--;
            if(health <= 0)
            {
                roomBossIsIn.enemiesInRoom.Remove(this.gameObject);    //Remove it from the enemyIsInRoom list associated with the Room this is a child of
                Destroy(this.gameObject);
            }
            
        }

    }
}
