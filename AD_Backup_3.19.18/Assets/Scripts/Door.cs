﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public enum Direction { North, South, East, West };

    public Direction dir;

    float X;
    float Y;
    private void Start()
    {
        X = this.transform.localPosition.x;
        Y = this.transform.localPosition.y;

        

        if ((X == 8f) && (Y == 9f))
            dir = Direction.North;
        else if ((X == 8f) && (Y == -1f))
            dir = Direction.South;
        else if ((X == 17f) && (Y == 4f))
            dir = Direction.East;
        else
            dir = Direction.West;
    
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            switch(dir)
            {
                case Direction.North:
                    Player.S.MoveToAdjRoom("North");
                    break;
                case Direction.South:
                    Player.S.MoveToAdjRoom("South");
                    break;
                case Direction.East:
                    Player.S.MoveToAdjRoom("East");
                    break;
                case Direction.West:
                    Player.S.MoveToAdjRoom("West");
                    break;
                default:
                    break;
            }
        }     
    }

    //private void OnTriggerExit2D(Collider2D collision)
    //{
    //    //Player.S.transitioningRooms = false;
    //}



    // Update is called once per frame
    void Update () {
		
	}
}