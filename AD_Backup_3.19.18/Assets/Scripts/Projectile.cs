﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private float speed = 15f;

    private Vector3 pz;
    Vector3 startingPosition;

    // Use this for initialization
    void Start ()
    {
        pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);           //Projectile will move in the direction of wherever the mouse is
        pz.z = -3;

        startingPosition = Player.S.transform.position;
    }

    // Update is called once per frame
    void Update ()
    {
        //pz = Camera.main.WorldToScreenPoint(Input.mousePosition);           //Projectile will move in the direction of wherever the mouse is
        //pz.z = -3;

        //Vector3 dir = (pz - Input.mousePosition).normalized;

        //GetComponent<Rigidbody>().AddForce(dir * speed);

        GetComponent<Rigidbody2D>().AddForce((pz - startingPosition).normalized * speed);
        //this.transform.position = Vector3.f(this.transform.position, pz, speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Getting hit with a weapon will destroy this game object
        
           // Debug.Log("Ouch");
           
            Destroy(this.gameObject);
        
    }

}
