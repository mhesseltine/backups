﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    private float speed = 25f;
    Vector3 startingPosition;
    Vector3 destination;

    // Use this for initialization
    void Start ()
    {
        destination = Player.S.transform.position;

        startingPosition = DinoBoss.S.transform.position;
        //startingPosition.y = startingPosition.y + 5;
    }
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<Rigidbody2D>().AddForce((destination - startingPosition).normalized * speed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Getting hit with a weapon will destroy this game object

        Debug.Log("Ouch");

        Destroy(this.gameObject);

    }
}
