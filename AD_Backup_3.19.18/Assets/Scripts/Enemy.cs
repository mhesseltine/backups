﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    protected float playerDamage;
    protected float enemySpeed;

    public Player target;

	// Use this for initialization
	protected virtual void Start ()
    {
        target = Player.S;
        enemySpeed = 2.5f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        
		
	}

    public void MovementPattern()
    {


    }
}
