﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    private int columns = 17;    //Number of x positions for the board
    private int rows = 9;        //Number of y positions for the board

    public int NumOfEnemies;


    private List<GameObject> roomDoors;

    public bool finalRoom = false;
    
    
    public bool doorsAreOpen;           //Checks if the doors are currently open
    public bool objectiveComplete;
    
    //Some code adapted from the open source Unity Tutorial for the 2D RogueLike

    public List<Vector3> gridPositions = new List<Vector3>();

    public GameObject[] floorTiles;     //Array to hold floor tiles
    public GameObject[] outerWallTiles; //Array to hold outer wall tiles
    public GameObject[] doorTiles;      //Array to hold door tiles
    public Sprite[] spriteTiles;        //Array to hold sprites for doors
    public List<GameObject> enemyPrefabs;       //List to hold enemies
    public List<GameObject> bossPrefabs;        //List to hold bosses
    public List<GameObject> enemiesInRoom;      //List to keep track of all enemies currently alive in the room    



    public bool playerInRoom = false;
    public GameObject Test;
    
    
    //[HideInInspector]
    //public GameObject board;

    //[HideInInspector]
    public int xCoord;  //X-Coordinate for the room (used for map layout)
    //[HideInInspector]
    public int yCoord;  //Y-Coordinate for the room (used for map layout)

    void InitializePositions()
    {
        gridPositions.Clear();
        for (int x = 1; x < columns-1; x++)
        {
            for (int y = 1; y < rows-1; y++)
            {
                gridPositions.Add(new Vector3(x, y, 0f));
            }
        }
    }

    private void Awake()
    {
        enemiesInRoom = new List<GameObject>();
        roomDoors = new List<GameObject>();
        doorsAreOpen = false;
        //objectiveComplete = true;       //Debugging only. Remove later.
        xCoord = GameManager.S.lastX;
        yCoord = GameManager.S.lastY;
        InitializePositions();
        SetUpCoords();
        GameManager.S.rooms.Add(this);

        NumOfEnemies = UnityEngine.Random.Range(2,5); //There will be either 2-4 enemies for every given room

       
    }

    public void SpawnEnemies()
    {
        List<Vector3> takenPositions = new List<Vector3>(); //Keeps track of which grid spaces will have an enemy spawn on them

        for (int i = 0; i < NumOfEnemies; i++)
        {
            Vector3 temp = gridPositions[UnityEngine.Random.Range(0, gridPositions.Count)];
            if (takenPositions.Contains(temp))
                i--;
            else
            {
                var enemyInstance = Instantiate(enemyPrefabs[0], this.transform.position + temp, Quaternion.identity, this.transform);
                enemiesInRoom.Add(enemyInstance);
                takenPositions.Add(temp);
            }
        }
    }

    public void SpawnBoss()
    {
        NumOfEnemies = 1;

        Vector3 temp = new Vector3(9, 4, -3);
        
        var enemyInstance = Instantiate(bossPrefabs[0], this.transform.position + temp, Quaternion.identity, this.transform);
        enemiesInRoom.Add(enemyInstance);
    }

    private void Update()
    {
        if (doorsAreOpen == false)
        {
            if(objectiveComplete == true)
            {
                foreach (var door in roomDoors)
                {
                    var spriteR = door.GetComponent<SpriteRenderer>();
                    spriteR.sprite = spriteTiles[1];

                    var doorBC2D = door.GetComponent<BoxCollider2D>();
                    doorBC2D.isTrigger = true;

                    var audiosrc = GetComponent<AudioSource>();
                    audiosrc.Play();
                }
                doorsAreOpen = true;
            }
        }

        if(enemiesInRoom.Count == 0)
            objectiveComplete = true;
    }


    public void Setup(bool north, bool south, bool east, bool west)
    {
        this.name = "Room_" + GameManager.S.currRoom++;
        for (int x = -1; x < columns + 1; x++)
        {
            for (int y = -1; y < rows + 1; y++)
            {
                GameObject tile = floorTiles[UnityEngine.Random.Range(0, floorTiles.Length)];   //Get a random tile in floorTiles
                bool tileIsDoor = false;    //Checks to see if the tile that was added is a door

                if (x == -1 || x == columns || y == -1 || y == rows)
                {
                    if (((x == 8) && north == true && y == rows) ||         //Should a door be on north wall?
                        ((x == 8) && south == true && y == -1) ||           //Should a door be on south wall?
                        ((y == 4) && east == true && x == columns) ||       //Should a door be on east wall?
                        ((y == 4) && west == true && x == -1))              //Should a door be on west wall?
                    {
                        tile = doorTiles[0];   //Door
                        tileIsDoor = true;
                    }  
                    else
                        tile = outerWallTiles[UnityEngine.Random.Range(0, outerWallTiles.Length)];        
                }
                GameObject tileInstance = Instantiate(tile, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;  //Instantiate desired tile
                tileInstance.transform.SetParent(this.transform);

                if (tileIsDoor == true)
                    roomDoors.Add(tileInstance);    //Add to roomDoors list. Makes it easy to open all doors at once
            }
        }
    }

    public void SetUpCoords()
    {
        int direction = UnityEngine.Random.Range(0, 4);  //Determine if x or y coord changes
        switch (direction)
        {
            case 0:
                while (CheckSpaceOccupied(xCoord, yCoord) == true)
                    xCoord++;
                break;
            case 1:
                while (CheckSpaceOccupied(xCoord, yCoord) == true)
                    yCoord++;
                break;
            case 2:
                while (CheckSpaceOccupied(xCoord, yCoord) == true)
                    xCoord--;
                break;
            case 3:
                while (CheckSpaceOccupied(xCoord, yCoord) == true)
                    yCoord--;
                break;
            default:
                break;
        }
        GameManager.S.lastX = xCoord;
        GameManager.S.lastY = yCoord;
    }

    public void TranslateRoom()
    {  
        //Takes the x and y coord of this room and translates it appropriately
        Vector3 newPos = new Vector3(xCoord * 19, yCoord * 11, 0);
        this.transform.position += newPos;
    }

    private bool CheckSpaceOccupied(int x, int y)
    {
        //Checks to see if this room's x and y coords match that of the params. Return true if so.
        foreach (var room in GameManager.S.rooms)
        {
            if (room.xCoord == x && room.yCoord == y)
                return true;
        }
        return false;   
    }  
}
